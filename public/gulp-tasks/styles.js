const {src, dest} = require("gulp");
const sass = require("gulp-sass");
const concat = require("gulp-concat");

const styles = () => {
    return src("./src/scss/*.scss")
        .pipe(sass().on("error", sass.logError))
        .pipe(concat("styles.min.css"))
        .pipe(dest("./dist/css/"));
};

const stylesMin = () => {
    return src("./src/scss/*.scss")
        .pipe(sass({outputStyle: "compressed"}).on("error", sass.logError))
        .pipe(concat("styles.min.css"))
        .pipe(dest("./dist/css/"));
};

exports.styles = styles;
exports.stylesMin = stylesMin;