const {src, dest} = require("gulp");
const fileInclude = require('gulp-file-include');


const includeHtml = () => {
    return src("./src/html/index.html")
        .pipe(fileInclude({
            prefix: "@@",
            basepath: "@file"
        }))
        .pipe(dest("./"));
};

exports.includeHtml = includeHtml;